<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
</head>
<body>
    <form action="?method=calcular" method="POST"> <!--En action indicamos la ruta y el nombre del archivo php-->
        <label>Calculadora<br></label>
        <input type="text" name="n1"><!--n1 y n2 son las opciones que queremos hacer lo que pongamos en la operacion-->
        <select name="lista">
            <option value="sumar">Sumar</option>
            <option value="restar">Restar</option>
            <option value="multiplicar">Multiplicar</option>
            <option value="dividir">Dividir</option>
        </select>
        <input type="text" name="n2">
        <input type="submit" value="Resultado"> <!--Boton de envío para ver resultado-->
        <label><?php echo isset($resultado)? $resultado : "" ?></label>
    </form>
</body>
</html>
