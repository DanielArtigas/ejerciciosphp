<!DOCTYPE html>
<html>
<head>
    <title>Galeria</title>
</head>
<body>
    <h1>Galeria de Imagenes</h1>
    <?php foreach ($array as $key => $imagen): ?>

        <img width="100" height="100" src=<?php echo "img/$imagen" ?>>
        <li><?php echo $imagen ?></li>
        <a>
            <form method="post" action="?method=detalle">
                <input type="hidden" name="detalle" value="<?php echo "img/$imagen" ?>">
                <input type="submit" value="Ver detalles">
            </form>
        </a>

        <a>
            <form method="post" action="?method=borrar">
                <input type="hidden" name="file" value="<?php echo "img/$imagen" ?>">
                <input type="submit" value="Borrar" target="_self"><br><br>
            </form>
        </a>

    <?php endforeach ?>

    <h3><a href="index.php?method=home"> Volver </a></h3>
</body>
</html>
