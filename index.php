<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios php</title>
</head>
<body>
<h1>Ejercicios PHP</h1>
<h2>Alumno: Daniel Artigas</h2>
Crea enlaces a tus ejercicios:
<ul>
    <li><a href="/ejercicio1.php"> Ejercicio 1</a></li>
    <li><a href="/ejercicio2.php"> Ejercicio 2</a></li>
    <li><a href="/ejercicio3.php"> Ejercicio 3</a></li>
    <li><a href="/ejercicio4.php"> Ejercicio 4</a></li>
    <li><a href="/ejercicio5.html"> Ejercicio 5</a></li>
    <li><a href="/ejercicio6.html"> Ejercicio 6</a></li>
    <li><a href="/ejercicio7.php"> Ejercicio 7</a></li>
    <li><a href="/ejercicio8.php"> Ejercicio 8</a></li>
    <li><a href="/cookies.php"> Ejercicio cookies</a></li>
    <li><a href="/calculadora/calculadora.php"> Calculadora</a></li>
    <li><a href="/galeria/viewHome.php"> Galeria Fotos</a></li>
    <li><a href="/loteria/viewLoteria.php"> Loteria</a></li>

</ul>
</body>
</html>
