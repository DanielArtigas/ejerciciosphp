<?php

class App {

    // Iniciamos la sesion y el array
    function __construct()
    {
        session_start();
        $apuesta = [];
    }


    public function loteria()
    {
        require "viewLoteria.php";
    }

    public function toogle()
    {

         // unset($_SESSION["mensaje"]);
         // exit();
        $apuesta = $_SESSION["apuesta"];
        $numero = $_REQUEST["numero"];

        foreach ($apuesta as $nr) {
            if($nr == $numero) {
                unset($apuesta["$numero"]);
                $_SESSION["apuesta"] = $apuesta;
                header("Location:index.php?method=loteria");
                return;
            }
        }
        $apuesta[$numero] = $numero;
        $_SESSION["apuesta"] = $apuesta;

        header("Location:index.php?method=loteria");

    }

    public function apostar()
    {
        if(count($_SESSION["apuesta"]) < 6 ) {
            $_SESSION["mensaje"] = "Necesitas 6 números para realizar la apuesta! y solo tienes " . count($_SESSION["apuesta"]);
            $_SESSION["valida"] = false;
        } else if (count($_SESSION["apuesta"]) == 6) {
            $_SESSION["mensaje"] ="Apuesta SIMPLE";
            $_SESSION["valida"] = true;
        } else {
            $_SESSION["mensaje"] = "Apuesta MULTIPLE";
            $_SESSION["valida"] = true;
        }
        $this->comprobarApuesta();



    }

    public function comprobarApuesta()
    {
        if(!$_SESSION["valida"]) {
            header("Location:index.php?method=loteria");
        } else {
            require "viewApuesta.php";
        }
    }

    public function reiniciar()
    {
        unset($_SESSION["apuesta"]);
        unset($_SESSION["mensaje"]);
        unset($_SESSION["valido"]);

        require "viewLoteria.php";

    }

}
