<?php

// Importamos la clase principal
require "App.php";

// Creamos una instancia de la clase App
$app = new App();

// Comprobamos si existe el method
if (isset($_GET["method"])) {
    $method = $_GET["method"];
}
// Si no existe redirigimos al login
else {
    $method = "loteria";
}

// Comprobamos la existencia del metodo introducido
if(!method_exists("App", $method)) {
    // Si no existe, redirigimos al metodo loteria
    $method = "loteria";
}

// Ejecutamos el method
$app->$method();
