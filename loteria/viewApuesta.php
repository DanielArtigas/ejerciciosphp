<!DOCTYPE html>
<html>
<head>
    <title>Apuesta</title>
</head>
<body>

    <?php
    // Comprobamos que la apuesta sea valida
    if($_SESSION["valida"]) {
    echo "<h1>$_SESSION[mensaje]</h1>";
    $i = 1;

    // Imprimimos todos los numeros de la apuesta
    foreach ($_SESSION["apuesta"] as  $nr) {
    echo "<h3><b> Número $i apuesta: $nr</b></h3> ";
    $i++;
}

}
?>
<!-- Creamos un enlace para reiniciar toda la apuesta -->
<a href="?method=reiniciar">Realizar otra apuesta!</a>

</body>
</html>
