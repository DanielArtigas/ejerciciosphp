<!DOCTYPE html>
<html>
<head>
  <title>Loteria</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <form method="post" action="?method=apostar">

    <!-- Comprobamos si existe mensaje y si es de error para mostrarlo -->
    <?php
    if(!empty($_SESSION["mensaje"]) && !$_SESSION["valida"] ) {
    echo "<b id=error>$_SESSION[mensaje]</b>";
  }
  ?>

  <!-- Mostramos los numeros elegidos -->
  <div id="apuesta">
    <?php
    if(!empty($_SESSION["apuesta"])) {
    echo "<h2>Numeros apostados: </h2>";
    foreach ($_SESSION["apuesta"] as  $nr) {
    echo "<b>$nr</b> ";
  }
}
?>
</div>

<!-- Boton para enviar la apuesta -->
<input type="submit" id="btnSend" value="Enviar apuesta">

<!-- Tabla con todos los numeros de la apuesta -->
<table border="1">
  <?php
  $numero = 1;

  // Creamos cada fila y columna mediante bucles
  for ($i=0; $i < 7; $i++) {
  echo "<tr>";
    for ($j=0; $j <7 ; $j++) {

    // Añadimos un enlace a cada numero que nos rediriga al metodo toogle
    echo "<td> <a href=?method=toogle&numero=$numero> $numero </a> </td>";
    $numero++;

  }
echo "</tr>";
}
?>
</table>

</form>

</body>
</html>
