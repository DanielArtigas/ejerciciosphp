<?php
/**
*
*/
class App
{

    function __construct()
    {
        //echo "App!!<br>";
        session_start();
    }
    public function login(){
        require 'viewLogin.php';
    }
    public function auth(){
        echo "Auth!!<br>";

        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])){
            $user = $_REQUEST['user'];
            $_SESSION['user'] = $user;
            header('Location:index.php?method=home');
        }else{
            header('Location:index.php?method=login');
        }
        return;
    }
    public function home(){
        if (!isset($_SESSION['user'])){
            header('Location:index.php?method=login');
            return;

        }
        $user = $_SESSION['user'];

        if (isset($_SESSION['deseos'])){
            $deseos = $_SESSION['deseos'];
        }else{
            $deseos = [];
        }
        $_SESSION['deseos'] = $deseos;
        require 'viewList.php';
    }

    public function new(){
        if(isset($_REQUEST['deseo']) && !empty($_REQUEST['deseo'])){
            $_SESSION['deseos'][] = $_REQUEST['deseo'];

           // $deseos = $_SESSION['deseos'];
           // $deseos[] = $_REQUEST['deseo'];
           // $_SESSION['deseos'] = $deseos;
            header('Location:index.php?method=home');
        }
        header('Location:index.php?method=home');
    }

    public function close(){
        //echo "Close!!";
        session_destroy();
        unset($_SESSION);
        header('Location:index.php?method=home');
    }
    public function delete(){
        echo "Delete $_REQUEST[key]!!";
        $key = (integer) $_REQUEST['key'];
        unset($_SESSION['deseos'][$key]);
        header('Location:index.php?method=home');
    }
    public function empty(){
        unset($_SESSION['deseos']);
        header('Location:index.php?method=home');
    }
}
