<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <h1>Lista de deseos (sesiones) de
        <?php echo isset($_SESSION['user']) ? $_SESSION['user'] : '' ?></h1>

       <p><a href="?method=close">Cerrar sesion</a></p>
        <h3>Nuevo</h3>

        <form method="post" action="?method=new">

            <label>Nuevo deseo</label>
            <input type="text" name="deseo">
            <input type="submit" name="enviar">
        </form>
        <hr>
        <h3>lista de deseos</h3>
        <ul>
            <?php foreach ($deseos as $clave => $deseo): ?>
                <li><?php echo $deseo ?></li>
                <a href="?method=delete&key=<?php  echo $clave?>">Borrar</a>
            <?php endforeach ?>
        </ul>
        <a href="?method=empty">Vaciar todo</a>
    </body>
    </html>
